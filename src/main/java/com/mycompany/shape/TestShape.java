/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shape;

/**
 *
 * @author sippu
 */
public class TestShape {
    public static void main(String[] args) {
        Rectangle rec1 = new Rectangle(6,9);
        System.out.println("Area = "+rec1.calAreaR());
        Circle cir1 = new Circle(6);
        System.out.println("Area = "+cir1.calAreaC());
        Triangle tri1 = new Triangle(6,9);
        System.out.println("Area = "+tri1.calAreaT());
        Square squ1 = new Square(6,6);
        System.out.println("Area ="+squ1.calAreaS());
        squ1.info();
    }
    
}
