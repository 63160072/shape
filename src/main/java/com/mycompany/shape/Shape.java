/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shape;

/**
 *
 * @author sippu
 */
public class Shape {

    private double l;
    private double w;
    private double r;
    double pi = 22.0 / 7;

    public Shape(double l, double w) {
        this.l = l;
        this.w = w;
    }

    public double calArea() {
        return l * w;
    }
    public Shape(double r) {
        this.r = r;
    }
    public double calAreaC() {
        return pi*r*r;
    }

}
