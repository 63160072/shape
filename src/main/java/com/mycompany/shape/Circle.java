/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shape;

/**
 *
 * @author sippu
 */
public class Circle {

    public Circle(double r) {
        this.r = r;
        if (r < 0) {
            System.out.println("Radius must be more than 0");
        }
    }
    private double r;
    public static final double pi = 22.0 / 7;

    public double calAreaC() {
        return pi * r * r;
    }

    public void setR(double r) {
        if (r <= 0) {
            System.out.println("Error: Radius must more than zero!!!");
            return;
        }
        this.r = r;
    }

    public double getR() {
        return r;
    }

    public void info() {
        System.out.println("Radius = " + r+"Area = "+calAreaC());
    }
}
