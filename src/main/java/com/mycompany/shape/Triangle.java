/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shape;

/**
 *
 * @author sippu
 */
public class Triangle {

    private double l;
    private double w;

    public Triangle(double l, double w) {
        this.l = l;
        this.w = w;
        if (l < 0) {
            System.out.println("Length must me more than 0");
        }
        if (w < 0) {
            System.out.println("Width must be more than 0");
        }
    }

    public double calAreaT() {
        return (l * w) * 0.5;
    }

    public double getL() {
        return l;

    }

    public double getW() {
        return w;

    }
    public void info(){
        System.out.println("Length = "+l+" Width = "+w+" Area = "+calAreaT());
    }
}
