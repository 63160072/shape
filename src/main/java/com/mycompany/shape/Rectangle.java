/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shape;

/**
 *
 * @author sippu
 */
public class Rectangle {

    protected double l;
    protected double w;

    public Rectangle(double l, double w) {
        this.l = l;
        this.w = w;
        if(l<0){
            System.out.println("Length must me more than 0");
        }
        if(w<0){
            System.out.println("Width must be more than 0");
        }
    }

    public double calAreaR() {
        return l * w;
    }

    public void setL(double l) {
        if (l <= 0) {
            System.out.println("Error: Lenghts must more than zero!!!");
            return;
        }
        this.l = l;
    }

    public void setW(double w) {
        if (w <= 0) {
            System.out.println("Error: Wides must more than zero!!!");
            return;
        }
        this.w = w;
    }
        public void info(){
        System.out.println("Length = "+l+" Width = "+w+" Area = "+calAreaR());
    }
}
